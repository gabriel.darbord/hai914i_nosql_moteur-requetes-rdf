# HAI914I NoSQL - moteur de requêtes RDF

Implémentation d’un mini moteur de requêtes en étoile avec l’approche hexastore pour l'interrogation de données RDF.

---

## Utilisation

```
java -jar rdfengine.jar [-compare] -data <filepath> -output <dirpath> -queries <filepath>
 -compare              comparaison entre notre moteur et Jena
 -data <filepath>      chemin du fichier de données
 -output <dirpath>     chemin du dossier de sortie du résultat
 -queries <dirpath>    chemin du dossier de requêtes
```

Exemple utilisant `sample_query.queryset` :
```
java -jar rdfengine.jar -data data/100K.nt -queries data/sample_query.queryset -output target
```

Workload complet :
```
java -jar rdfengine.jar -data data/100K.nt -queries data/STAR_ALL_workload.queryset -output target
```

---

## Documentation

Javadoc : <https://gabriel.darbord.gitlab.io/hai914i_nosql_moteur-requetes-rdf/>  
Rapport de projet : <https://www.overleaf.com/read/dcwdvwxwmbzd>
