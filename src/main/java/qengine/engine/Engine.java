package qengine.engine;

import java.io.IOException;
import java.util.Set;

/**
 * Un moteur de requêtes RDF.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public interface Engine {

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	/**
	 * Retourne la taille de la base de données utilisée par ce moteur.
	 * 
	 * @return la taille de la base de données utilisée par ce moteur
	 */
	int dictSize();

	/**
	 * Retourne la taille de l'indexage de la base de données.
	 * 
	 * @return la taille de l'indexage de la base de données
	 */
	int indexSize();

	/**
	 * Retourne le nombre de triplets RDF analysés.
	 * 
	 * @return le nombre de triplets RDF analysés
	 */
	int nbParsedData();

	/**
	 * Retourne le nombre de requêtes traitées.
	 * 
	 * @return le nombre de requêtes traitées
	 */
	int nbProcessedQueries();

	/*
	 * =============================================================================
	 * === Analyse des données
	 * =============================================================================
	 */

	/**
	 * Récupère et analyse les données au format RDF contenues dans le fichier.
	 * 
	 * @param dataPath le chemin du fichier contenant les données
	 * @throws IOException
	 */
	void parseData(String dataPath) throws IOException;

	/*
	 * =============================================================================
	 * === Traitement des requêtes
	 * =============================================================================
	 */

	/**
	 * Récupère et analyse les requêtes au format RDF contenues dans une
	 * arborescence de fichiers.
	 * 
	 * @param queryPath  le chemin du fichier ou dossier contenant les requêtes
	 * @param outputPath le chemin du fichier résultat qui sera généré
	 * @throws IOException
	 */
	void parseQueries(String queryPath, String outputPath) throws IOException;

	/**
	 * Traitement d'une requête RDF.
	 * 
	 * @param queryString la requête à traiter
	 * @return l'ensemble des résultats de la requête
	 */
	Set<?> processQuery(String queryString);

}
