package qengine.engine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.algebra.StatementPattern;
import org.eclipse.rdf4j.query.algebra.helpers.StatementPatternCollector;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.eclipse.rdf4j.query.parser.sparql.SPARQLParser;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.ContextStatementCollector;

import qengine.program.Stopwatch;
import qengine.query.projection.ProjectionUtils;

/**
 * Moteur de requêtes utilisant RDF4J.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class RDF4JEngine extends AbstractEngine {
	private static final SPARQLParser sparqlParser = new SPARQLParser();

	private Model model;

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	@Override
	public int dictSize() {
		return model.size();
	}

	@Override
	public int indexSize() {
		// un Model n'a pas d'indexage
		return 0;
	}

	@Override
	public int nbParsedData() {
		// chaque triplet analysé est contenu dans le modèle
		return (int) model.size();
	}

	/*
	 * =============================================================================
	 * === Analyse des données
	 * =============================================================================
	 */

	@Override
	public void parseData(String dataFile) throws IOException {
		try (Reader dataReader = new BufferedReader(new FileReader(dataFile))) {
			// On va parser des données au format ntriples
			RDFParser rdfParser = Rio.createParser(RDFFormat.NTRIPLES);

			// On utilise un handler
			ContextStatementCollector handler = new ContextStatementCollector(SimpleValueFactory.getInstance(),
					(Resource) null);
			rdfParser.setRDFHandler(handler);

			// Parsing et traitement de chaque triple par le handler
			rdfParser.parse(dataReader, BASE_URI);

			// Création du modèle à partir des déclarations
			Stopwatch.push("dictionaryCreation");
			model = new LinkedHashModel(handler.getStatements());
			Stopwatch.pop();

			Stopwatch.put("indexCreation", 0); // pas de création d'index
		}
	}

	/*
	 * =============================================================================
	 * === Traitement des requêtes
	 * =============================================================================
	 */

	@Override
	public Set<Resource> processQuery(String queryString) {
		// analyse de la requête
		ParsedQuery parsedQuery = sparqlParser.parseQuery(queryString, null);

		// récupération des patrons (triplets <s,p,o>) de la requête
		List<StatementPattern> patterns = StatementPatternCollector.process(parsedQuery.getTupleExpr());

		// création de la liste des projections
		List<Set<Resource>> projections = new ArrayList<>(patterns.size());

		// récupération des projections
		for (int i = 0; i < patterns.size(); i++) {
			// récupération du prédicat et de l'objet
			IRI pred = (IRI) patterns.get(i).getPredicateVar().getValue();
			Value obj = patterns.get(i).getObjectVar().getValue();

			// projection des sujets
			Set<Resource> projection = model.filter(null, pred, obj, (Resource) null).subjects();
			projections.add(projection);
		}

		// jointure des projections
		return ProjectionUtils.join(projections);
	}

}
