package qengine.engine.comparator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;
import java.util.function.BiFunction;

import qengine.engine.AbstractEngine;
import qengine.engine.Engine;

/**
 * Comparaison de deux moteurs en les faisant s'exécuter séquentiellement.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public abstract class AbstractEngineComparator extends AbstractEngine {

	/*
	 * =============================================================================
	 * === Attributs
	 * =============================================================================
	 */

	protected Engine left, right;
	protected String leftName, rightName;
	protected boolean leftDoneParsingQueries = false;

	/**
	 * Énumération des implémentations de {@link AbstractEngineComparator}.
	 * Possibilité d'instancier avec
	 * {@link #get(qengine.engine.AbstractEngine.Impl, qengine.engine.AbstractEngine.Impl)
	 * get(AbstractEngine.Impl, AbstractEngine.Impl)}.
	 */
	public enum Impl {
		/**
		 * {@link DefaultEngineComparator}
		 */
		DEFAULT(DefaultEngineComparator::new),
		/**
		 * {@link VerboseEngineComparator}
		 */
		VERBOSE(VerboseEngineComparator::new);

		private BiFunction<AbstractEngine.Impl, AbstractEngine.Impl, Engine> supplier;

		Impl(BiFunction<AbstractEngine.Impl, AbstractEngine.Impl, Engine> supplier) {
			this.supplier = supplier;
		}

		/**
		 * Retourne une instance du type représenté par ce membre de l'énumération.
		 * 
		 * @return une instance du type représenté par ce membre de l'énumération
		 */
		public Engine get(AbstractEngine.Impl left, AbstractEngine.Impl right) {
			return supplier.apply(left, right);
		}
	}

	/*
	 * =============================================================================
	 * === Constructeur
	 * =============================================================================
	 */

	public AbstractEngineComparator(AbstractEngine.Impl left, AbstractEngine.Impl right) {
		this.left = left.get();
		this.right = right.get();
		leftName = left.name();
		rightName = right.name();
		System.out.println("===== COMPARING " + leftName + " AND " + rightName);
	}

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	@Override
	public int dictSize() {
		return left.dictSize() + right.dictSize();
	}

	@Override
	public int indexSize() {
		return left.indexSize() + right.indexSize();
	}

	@Override
	public int nbParsedData() {
		return left.nbParsedData() + right.nbParsedData();
	}

	@Override
	public int nbProcessedQueries() {
		return nbProcessedQueries * 2;
	}

	/*
	 * =============================================================================
	 * === Analyse des données
	 * =============================================================================
	 */

	@Override
	public void parseData(String dataPath) throws IOException {
		System.out.println("=== PARSING DATA");
		left.parseData(dataPath);
		right.parseData(dataPath);
	}

	/*
	 * =============================================================================
	 * === Traitement des requêtes
	 * =============================================================================
	 */

	@Override
	public void parseQueries(String queryPath, String outputPath) throws IOException {
		System.out.println("=== PROCESSING QUERIES");
		super.parseQueries(queryPath, outputPath);
		System.out.println("=== ALL DONE!");
	}

	/**
	 * Un comparateur n'analyse pas de requête, cela est délégué aux moteurs à
	 * comparer.
	 */
	public final Object parseQuery(String queryString) {
		return null;
	}

	/**
	 * Redéfinition du wrapper qui doit être implémenté de sorte à appeler
	 * {@link Engine#processQuery(String) processQuery(String)} sur les moteurs à
	 * comparer.
	 */
	@Override
	protected abstract void processQuery(String queryString, FileWriter fileWriter) throws IOException;

	/**
	 * Un comparateur ne traite pas de requête, cela est délégué aux moteurs à
	 * comparer. Ces appels sont à effectuer dans le wrapper
	 * {@link #processQuery(String, FileWriter)}.
	 * 
	 * @return null
	 */
	@Override
	public final Set<?> processQuery(String queryString) {
		return null;
	}

}
