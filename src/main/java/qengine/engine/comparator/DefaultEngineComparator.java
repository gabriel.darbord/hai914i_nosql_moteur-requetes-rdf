package qengine.engine.comparator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import qengine.engine.AbstractEngine;

/**
 * Comparaison textuelle dans un fichier généré en sortie.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class DefaultEngineComparator extends AbstractEngineComparator {

	/*
	 * =============================================================================
	 * === Constructeur
	 * =============================================================================
	 */

	public DefaultEngineComparator(AbstractEngine.Impl left, AbstractEngine.Impl right) {
		super(left, right);
	}

	/*
	 * =============================================================================
	 * === Traitement des requêtes
	 * =============================================================================
	 */

	@Override
	public void processQuery(String queryString, FileWriter fileWriter) throws IOException {
		// récupération des résultats
		Set<?> leftResult = left.processQuery(queryString);
		Set<?> rightResult = right.processQuery(queryString);

		// écriture des résultats dans le fichier
		fileWriter.write(queryString + ";{" + leftName + ":" + leftResult + "," + rightName + ":" + rightResult + "}"
				+ System.lineSeparator());

		// vérification de la complétude et consistance des deux résultats
		if (leftResult.size() != rightResult.size() || !leftResult.containsAll(rightResult)) {
			System.err.println("/!\\ Found different results for query: " + System.lineSeparator() + queryString);
		}
	}

}
