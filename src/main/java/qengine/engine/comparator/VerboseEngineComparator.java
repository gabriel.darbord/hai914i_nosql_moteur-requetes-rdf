package qengine.engine.comparator;

import java.io.FileWriter;
import java.io.IOException;

import qengine.engine.AbstractEngine;

/**
 * Comparaison textuelle dans le terminal.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class VerboseEngineComparator extends AbstractEngineComparator {
	private static final String LN = System.lineSeparator();

	/*
	 * =============================================================================
	 * === Constructeur
	 * =============================================================================
	 */

	public VerboseEngineComparator(AbstractEngine.Impl left, AbstractEngine.Impl right) {
		super(left, right);
	}

	/*
	 * =============================================================================
	 * === Analyse des données
	 * =============================================================================
	 */

	@Override
	public void parseData(String dataPath) throws IOException {
		super.parseData(dataPath);
		System.out.println("=== " + leftName + LN//
				+ " dictionary size: " + left.dictSize() + " index size: " + left.indexSize() + LN//
				+ "=== " + rightName + LN//
				+ " dictionary size: " + right.dictSize() + " index size: " + right.indexSize() + LN);
	}

	/*
	 * =============================================================================
	 * === Traitement des requêtes
	 * =============================================================================
	 */

	@Override
	public void processQuery(String queryString, FileWriter fileWriter) throws IOException {
		System.out.println(LN + "=== " + leftName + LN + left.processQuery(queryString));
		System.out.println("=== " + rightName + LN + right.processQuery(queryString));
	}

}
