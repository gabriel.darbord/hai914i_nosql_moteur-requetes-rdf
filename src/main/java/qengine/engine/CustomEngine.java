package qengine.engine;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.parser.ParsedQuery;
import org.eclipse.rdf4j.query.parser.sparql.SPARQLParser;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFHandler;

import qengine.dictionary.Dictionary;
import qengine.dictionary.HashMapIndexedDictionary;
import qengine.index.Hexastore;
import qengine.index.Index;
import qengine.program.Stopwatch;
import qengine.query.pattern.Pattern;
import qengine.query.pattern.collector.SubjectPatternCollector;
import qengine.query.projection.ProjectionUtils;

/**
 * Moteur de requêtes RDF très simplifié, créé dans le cadre de ce projet.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class CustomEngine extends AbstractEngine {
	private static final SPARQLParser sparqlParser = new SPARQLParser();

	private Dictionary dictionary;
	private Index index;
	private int nbParsedData;

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	@Override
	public int dictSize() {
		return dictionary.size();
	}

	@Override
	public int indexSize() {
		return index.size();
	}

	@Override
	public int nbParsedData() {
		return nbParsedData;
	}

	/*
	 * =============================================================================
	 * === Analyse des données
	 * =============================================================================
	 */

	@Override
	public void parseData(String dataPath) throws IOException {
		try (Reader dataReader = new BufferedReader(new FileReader(dataPath))) {
			// on va parser des données au format ntriples
			RDFParser rdfParser = Rio.createParser(RDFFormat.NTRIPLES);

			// instanciation du dictionnaire et de l'index
			dictionary = new HashMapIndexedDictionary();
			index = new Hexastore();

			// on utilise notre implémentation de handler
			rdfParser.setRDFHandler(new AbstractRDFHandler() {
				@Override
				public void handleStatement(Statement st) {
					// récupération du sujet, prédicat et objet
					String subject = st.getSubject().stringValue();
					String predicate = st.getPredicate().stringValue();
					String object = st.getObject().stringValue();

					// récupération de l'identifiant ou création d'une entrée pour chacun
					Stopwatch.push("dictionaryCreation");
					int subjectId = dictionary.addIfAbsent(subject);
					int predicateId = dictionary.addIfAbsent(predicate);
					int objectId = dictionary.addIfAbsent(object);
					Stopwatch.pop();

					// remplissage de l'index
					Stopwatch.push("indexCreation");
					index.add(subjectId, predicateId, objectId);
					Stopwatch.pop();

					// incrémentation du nombre de triplets analysés
					++nbParsedData;
				}
			});

			// parsing et traitement de chaque triple par le handler
			rdfParser.parse(dataReader, BASE_URI);
		}
	}

	/*
	 * =============================================================================
	 * === Traitement des requêtes
	 * =============================================================================
	 */

	@Override
	public Set<String> processQuery(String queryString) {
		// analyse de la requête
		ParsedQuery parsedQuery = sparqlParser.parseQuery(queryString, null);

		// récupération des patrons (triplets <s,p,o>) de la requête
		List<Pattern> patterns = SubjectPatternCollector.process(parsedQuery.getTupleExpr(), dictionary);

		// création de la liste des projections
		List<Set<Integer>> projections = new ArrayList<>(patterns.size());

		// récupération de la projection des sujets de chaque patron
		for (int i = 0; i < patterns.size(); i++) {
			Set<Integer> projection = index.project(patterns.get(i));
			projections.add(projection);
		}

		// jointure des projections
		Set<Integer> results = ProjectionUtils.join(projections);

		// traduction des indices en données
		return dictionary.translate(results);
	}

}
