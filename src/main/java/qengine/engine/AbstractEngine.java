package qengine.engine;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

import qengine.program.Stopwatch;

/**
 * Base d'un moteur de requêtes RDF.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public abstract class AbstractEngine implements Engine {

	/*
	 * =============================================================================
	 * === Attributs
	 * =============================================================================
	 */

	protected static final String BASE_URI = null;

	/**
	 * Énumération des implémentations de {@link AbstractEngine}. Possibilité
	 * d'instancier avec {@link #get()}.
	 */
	public enum Impl {
		/**
		 * {@link CustomEngine}
		 */
		CUSTOM(CustomEngine::new),
		/**
		 * {@link RDF4JEngine}
		 */
		RDF4J(RDF4JEngine::new),
		/**
		 * {@link JenaEngine}
		 */
		JENA(JenaEngine::new);

		private Supplier<Engine> supplier;

		Impl(Supplier<Engine> supplier) {
			this.supplier = supplier;
		}

		/**
		 * Retourne une instance du type représenté par ce membre de l'énumération.
		 * 
		 * @return une instance du type représenté par ce membre de l'énumération
		 */
		public Engine get() {
			return supplier.get();
		}
	}

	/**
	 * Compteur de requêtes traitées.
	 */
	protected int nbProcessedQueries;

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	@Override
	public int nbProcessedQueries() {
		return nbProcessedQueries;
	}

	/*
	 * =============================================================================
	 * === Traitement des requêtes
	 * =============================================================================
	 */

	public void parseQueries(String queryPath, String outputPath) throws IOException {
		// récupération des fichiers de requêtes
		ArrayList<File> queryFiles;
		File file = new File(queryPath);

		if (file.isDirectory()) {
			queryFiles = listQueryFiles(file);
		} else {
			queryFiles = new ArrayList<>(1);
			queryFiles.add(file);
		}

		// ouverture d'un écrivain sur le fichier de sortie
		try (FileWriter fileWriter = new FileWriter(outputPath)) {
			// écriture des en-têtes
			fileWriter.write("Requête;Résultats" + System.lineSeparator());

			// buffer pour récupérer la requête
			StringBuilder queryString = new StringBuilder();

			// itération sur les fichiers de requêtes
			for (File queryFile : queryFiles) {
				/*
				 * on utilise un stream pour lire les lignes une par une, sans avoir à toutes
				 * les stocker entièrement dans une collection
				 */
				try (Stream<String> lineStream = Files.lines(queryFile.toPath())) {
					Iterator<String> lineIterator = lineStream.iterator();

					/*
					 * on stocke plusieurs lignes jusqu'à ce que l'une d'entre elles se termine par
					 * un '}', on considère alors que c'est la fin d'une requête
					 */
					while (lineIterator.hasNext()) {
						String line = lineIterator.next();
						queryString.append(line);

						if (line.stripTrailing().endsWith("}")) {
							// traitement de la requête
							Stopwatch.pause(); // pause de queryParsing
							processQuery(queryString.toString(), fileWriter);
							Stopwatch.unpause();

							// reset le buffer de la requête en chaine vide
							queryString.setLength(0);

							// incrémentation du compteur de requêtes traitées
							++nbProcessedQueries;
						}
					}
				}
			}
		}
	}

	/**
	 * Wrapper de la fonction {@link #processQuery(String)} qui écrit le résultat
	 * dans un fichier.
	 * 
	 * @param queryString la requête à traiter
	 * @param fileWriter  un écrivain ouvert sur un fichier
	 */
	protected void processQuery(String queryString, FileWriter fileWriter) throws IOException {
		// traitement de la requête par l'implémentation
		Set<?> results = processQuery(queryString);

		// écriture du résultat dans le fichier
		final String ln = System.lineSeparator();
		fileWriter.write(queryString + ";" + results + ln);
	}

	/**
	 * Méthode utilitaire pour lister les fichiers de requêtes dans un dossier.
	 * 
	 * @param folder un dossier contenant des fichiers de requêtes
	 * @return la liste des fichiers de requêtes
	 */
	protected static ArrayList<File> listQueryFiles(File folder) {
		ArrayList<File> javaFiles = new ArrayList<File>();

		for (File file : folder.listFiles()) {
			if (file.isDirectory()) {
				javaFiles.addAll(listQueryFiles(file));
			} else if (file.getName().endsWith(".queryset")) {
				javaFiles.add(file);
			}
		}

		return javaFiles;
	}

}
