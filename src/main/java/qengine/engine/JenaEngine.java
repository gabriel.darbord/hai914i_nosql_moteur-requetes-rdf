package qengine.engine;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import qengine.program.Stopwatch;

/**
 * Moteur de requêtes utilisant Jena.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class JenaEngine extends AbstractEngine {
	private Model model;

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	@Override
	public int dictSize() {
		return (int) model.size();
	}

	@Override
	public int indexSize() {
		// un Model n'a pas d'indexage
		return 0;
	}

	@Override
	public int nbParsedData() {
		// chaque triplet analysé est contenu dans le modèle
		return (int) model.size();
	}

	/*
	 * =============================================================================
	 * === Analyse des données
	 * =============================================================================
	 */

	@Override
	public void parseData(String dataPath) throws IOException {
		// création du modèle
		model = ModelFactory.createDefaultModel();

		// lecture des données depuis le fichier
		try (FileInputStream in = new FileInputStream(dataPath)) {
			Stopwatch.start("dictionaryCreation");
			model.read(in, null, "N-TRIPLE");
			Stopwatch.stop("dictionaryCreation");
		}

		Stopwatch.put("indexCreation", 0); // pas de création d'index
	}

	/*
	 * =============================================================================
	 * === Traitement des requêtes
	 * =============================================================================
	 */

	@Override
	public Set<?> processQuery(String queryString) {
		// déballage de la requête
		Query query = QueryFactory.create(queryString);

		// exécution de la requête
		ResultSet queryResult = QueryExecutionFactory.create(query, model).execSelect();

		// récupération des résultats
		Set<String> results = new HashSet<>();
		while (queryResult.hasNext()) {
			results.add(queryResult.next().get("v0").toString());
		}

		return results;
	}

}
