package qengine.dictionary;

import java.util.Set;

/**
 * Dictionnaire des entrées d'une base de données au format RDF.
 * <p>
 * Il peut retrouver à la fois l'identifiant d'une donnée, et une donnée par son
 * identifiant.
 * <p>
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public interface Dictionary {

	/*
	 * =============================================================================
	 * === Accesseurs en écriture
	 * =============================================================================
	 */

	/**
	 * Ajoute la donnée si elle n'est pas déjà présente.
	 * 
	 * @return l'indice de la donnée
	 */
	int addIfAbsent(String data);

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	/**
	 * Retourne la taille du dictionnaire.
	 * 
	 * @return la taille de ce dictionnaire
	 */
	int size();

	/**
	 * Retrouve la donnée dans la liste.
	 * 
	 * @param index un indice
	 * @return la donnée à l'indice
	 */
	String getData(int index);

	/**
	 * Retrouve l'indice de la donnée.
	 * 
	 * @param data une donnée
	 * @return l'indice associé à la donnée
	 */
	int getIndex(String data);

	/**
	 * Traduit un ensemble d'indices en un ensemble de données.
	 * 
	 * @param projection un ensemble d'indices
	 * @return les données associées aux indices
	 */
	Set<String> translate(Set<Integer> projection);

}
