/**
 * Dictionnaires d'une base de données au format RDF.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
package qengine.dictionary;
