package qengine.dictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Cette implémentation utilise une liste contenant les données et une carte
 * d'une donnée vers son indice dans la liste.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class HashMapIndexedDictionary implements Dictionary {
	/**
	 * Liste des données, accessible par indice.
	 */
	private ArrayList<String> indexedData = new ArrayList<>();

	/**
	 * Carte d'une donnée vers son indice.
	 */
	private HashMap<String, Integer> dataIndex = new HashMap<>();

	/*
	 * =============================================================================
	 * === Accesseurs en écriture
	 * =============================================================================
	 */

	@Override
	public int addIfAbsent(String data) {
		Integer id = dataIndex.get(data);

		if (id == null) {
			id = indexedData.size();
			indexedData.add(data);
			dataIndex.put(data, id);
		}

		return id;
	}

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	@Override
	public int size() {
		return indexedData.size();
	}

	@Override
	public String getData(int index) {
		return indexedData.get(index);
	}

	@Override
	public int getIndex(String data) {
		return dataIndex.getOrDefault(data, -1);
	}

	@Override
	public Set<String> translate(Set<Integer> projections) {
		Set<String> translations = new HashSet<>(projections.size());

		for (Integer projection : projections) {
			String translation = getData(projection);
			translations.add(translation);
		}

		return translations;
	}

	@Override
	public String toString() {
		return "Dictionary" + dataIndex;
	}

}
