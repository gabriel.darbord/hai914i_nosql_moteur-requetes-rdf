package qengine.index.triplestore;

import java.util.Set;

import qengine.query.pattern.Pattern;

/**
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class SPO extends TripleStore {
	/*
	 * l'ordre par défaut du triplet correspond à l'ordre de ce store de ce fait la
	 * méthode add() n'a pas besoin d'être remplacée
	 */

	@Override
	public Set<Integer> project(Pattern pattern) {
		return project(pattern.getSubject(), pattern.getPredicate());
	}

}