package qengine.index.triplestore;

import java.util.Set;

import qengine.query.pattern.Pattern;

/**
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class POS extends TripleStore {
	@Override
	public boolean add(int subject, int predicate, int object) {
		return super.add(predicate, object, subject);
	}

	@Override
	public Set<Integer> project(Pattern pattern) {
		return project(pattern.getPredicate(), pattern.getObject());
	}
}
