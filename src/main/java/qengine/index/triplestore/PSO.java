package qengine.index.triplestore;

import java.util.Set;

import qengine.query.pattern.Pattern;

/**
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class PSO extends TripleStore {
	@Override
	public boolean add(int subject, int predicate, int object) {
		return super.add(predicate, subject, object);
	}

	@Override
	public Set<Integer> project(Pattern pattern) {
		return project(pattern.getPredicate(), pattern.getSubject());
	}
}
