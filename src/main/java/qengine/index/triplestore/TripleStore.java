package qengine.index.triplestore;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import qengine.index.Index;

/**
 * Indexage de triplets. L'ordre utilisé est spécifié par l'implémentation.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public abstract class TripleStore implements Index {
	/**
	 * Chaînage de deux cartes vers un ensemble.
	 */
	private Map<Integer, Map<Integer, Set<Integer>>> index = new HashMap<>();

	/*
	 * =============================================================================
	 * === Accesseurs en écriture
	 * =============================================================================
	 */

	/**
	 * Ajoute un triplet d'indices à l'index s'il n'est pas déjà présent.
	 * 
	 * @param first  première clé
	 * @param second seconde clé
	 * @param third  un élément associé aux clés
	 * @return vrai s'il a été ajouté
	 */
	@Override
	public boolean add(int first, int second, int third) {
		// récupération de l'ensemble associé aux clés
		Set<Integer> thirds = index//
				.computeIfAbsent(first, (key) -> new HashMap<>())//
				.computeIfAbsent(second, (key) -> new HashSet<>());

		// recherche du troisième élément
		boolean isNew = !thirds.contains(third);

		// insertion si absent
		if (isNew) {
			thirds.add(third);
		}

		return isNew;
	}

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	@Override
	public int size() {
		/*
		 * Bien que cela ne reflète pas réellement la taille totale d'index, elle peut
		 * être calculée en sommant cet appel sur des permutations utilisant un premier
		 * élément différent, par exemple SPO.size() + POS.size() + OSP.size(). Ce n'est
		 * pas très propre de déléguer ainsi mais nous n'avons jamais vraiment besoin de
		 * ce résultat de toute façon.
		 */
		return index.size();
	}

	/**
	 * La projection se fait véritablement dans cette méthode qui est appelée par
	 * l'implémentation de {@link #project(qengine.pattern.Pattern)
	 * project(Pattern)}. Les paramètres sont obtenus depuis une permutation des
	 * éléments du patron.
	 * 
	 * @param first  première clé
	 * @param second seconde clé
	 * @return les éléments associés aux clés
	 */
	protected Set<Integer> project(int first, int second) {
		Map<Integer, Set<Integer>> seconds = index.get(first);

		if (seconds == null) {
			return Collections.emptySet();
		}

		return seconds.getOrDefault(second, Collections.emptySet());
	}

	@Override
	public String toString() {
		return index.toString();
	}

}
