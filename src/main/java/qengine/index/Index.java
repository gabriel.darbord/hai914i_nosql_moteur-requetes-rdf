package qengine.index;

import java.util.Set;

import qengine.query.pattern.Pattern;

/**
 * Indexage d'un {@link qengine.dictionary.Dictionary Dictionary}.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public interface Index {

	/*
	 * =============================================================================
	 * === Accesseurs en écriture
	 * =============================================================================
	 */

	/**
	 * Ajoute un triplet d'indices à l'index s'il n'est pas déjà présent.
	 * 
	 * @param subject   un sujet
	 * @param predicate un prédicat
	 * @param object    un objet
	 * @return vrai s'il a été ajouté
	 */
	boolean add(int subject, int predicate, int object);

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	/**
	 * Retourne la taille de l'indexage.
	 * 
	 * @return la taille de l'indexage
	 */
	int size();

	/**
	 * Effectue la projection d'un patron.
	 * 
	 * @param pattern un triplet avec une inconnue
	 * @return l'ensemble des valeurs possibles pour l'inconnue
	 */
	Set<Integer> project(Pattern pattern);

}
