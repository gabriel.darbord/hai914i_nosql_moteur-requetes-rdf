/**
 * Indexages pour {@link qengine.dictionary}.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
package qengine.index;
