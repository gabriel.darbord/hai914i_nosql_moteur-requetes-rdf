package qengine.index;

import java.util.Set;

import qengine.index.triplestore.*;
import qengine.query.pattern.Pattern;

/**
 * Indexage d'un dictionnaire avec un hexastore, constitué de toutes les
 * permutations possibles d'un {@link TripleStore}. Cela va permettre de
 * retrouver une projection (deux constantes et une variable) en temps constant.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class Hexastore implements Index {

	/*
	 * =============================================================================
	 * === Attributs
	 * =============================================================================
	 */

	// hexastore constitué de toutes les permutations possibles d'un triplestore
	private final TripleStore OPS = new OPS();
	private final TripleStore OSP = new OSP();
	private final TripleStore POS = new POS();
	private final TripleStore PSO = new PSO();
	private final TripleStore SOP = new SOP();
	private final TripleStore SPO = new SPO();

	/*
	 * =============================================================================
	 * === Accesseurs
	 * =============================================================================
	 */

	@Override
	public int size() {
		/*
		 * Voir le commentaire de TripleStore#size() pour la raison de la somme. Cette
		 * dernière correspond à la taille d'un seul des TripleStore, et comme il s'agit
		 * de permutations ils ont tous la même taille. Enfin comme un hexastore possède
		 * six TripleStores, on multiplie par 6 pour obtenir la taille totale.
		 */
		return (SPO.size() + POS.size() + OSP.size()) * 6;
	}

	@Override
	public boolean add(int subject, int predicate, int object) {
		// vérifie si le triplet existe déjà dans un store
		// dans ce cas il existe aussi dans les autres stores
		// et ce n'est pas la peine de réessayer sur les suivants
		if (!OPS.add(subject, predicate, object)) {
			return false;
		}

		// ajout du triplet aux stores suivants
		OSP.add(subject, predicate, object);
		POS.add(subject, predicate, object);
		PSO.add(subject, predicate, object);
		SOP.add(subject, predicate, object);
		SPO.add(subject, predicate, object);
		// ...bien qu'on ne s'en serve jamais

		return true;
	}

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	@Override
	public Set<Integer> project(Pattern pattern) {
		/*
		 * TODO choisir le store selon les projections peut-être ? utilisation de OPS en
		 * dur pour le moment
		 */
		return OPS.project(pattern);
	}

	@Override
	public String toString() {
		final String ln = System.lineSeparator();
		return "Index[" + ln//
				+ "OPS" + OPS + ln//
				+ "OSP" + OSP + ln//
				+ "POS" + POS + ln//
				+ "PSO" + PSO + ln//
				+ "SOP" + SOP + ln//
				+ "SPO" + SPO + ln//
				+ "]";
	}

}
