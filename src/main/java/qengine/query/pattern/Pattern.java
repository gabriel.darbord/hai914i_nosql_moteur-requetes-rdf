package qengine.query.pattern;

/**
 * Traduction en triplet d'indices d'un
 * {@link org.eclipse.rdf4j.query.algebra.StatementPattern
 * org.eclipse.rdf4j.query.algebra.StatementPattern}. Ces indices sont obtenus
 * depuis un {@link qengine.dictionary.Dictionary Dictionary} et sont à utiliser
 * sur un {@link qengine.index.Index Index}.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public interface Pattern {

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	/**
	 * Retourne l'identifiant du sujet.
	 * 
	 * @return l'identifiant du sujet
	 */
	int getSubject();

	/**
	 * Retourne l'identifiant du prédicat.
	 * 
	 * @return l'identifiant du prédicat
	 */
	int getPredicate();

	/**
	 * Retourne l'identifiant de l'objet.
	 * 
	 * @return l'identifiant de l'objet
	 */
	int getObject();

}
