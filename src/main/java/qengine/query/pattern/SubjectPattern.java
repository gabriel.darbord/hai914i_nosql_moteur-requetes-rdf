package qengine.query.pattern;

/**
 * Patron contenant une projection sur le sujet.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class SubjectPattern implements Pattern {
	private int predicate, object;

	/*
	 * =============================================================================
	 * === Constructeur
	 * =============================================================================
	 */

	/**
	 * @param predicate le prédicat
	 * @param object    l'objet
	 */
	public SubjectPattern(int predicate, int object) {
		this.predicate = predicate;
		this.object = object;
	}

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	/**
	 * Ce patron concerne le sujet, donc pas de valeur valide rendue.
	 * 
	 * @return -1
	 */
	@Override
	public int getSubject() {
		return -1;
	}

	@Override
	public int getPredicate() {
		return predicate;
	}

	@Override
	public int getObject() {
		return object;
	}

	@Override
	public String toString() {
		return "<?, " + predicate + ", " + object + ">";
	}

}
