package qengine.query.pattern.collector;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.query.algebra.QueryModelNode;
import org.eclipse.rdf4j.query.algebra.StatementPattern;
import org.eclipse.rdf4j.query.algebra.helpers.AbstractQueryModelVisitor;

import qengine.dictionary.Dictionary;
import qengine.query.pattern.Pattern;
import qengine.query.pattern.SubjectPattern;

/**
 * Un {@link org.eclipse.rdf4j.query.algebra.QueryModelVisitor} qui collectionne
 * les traductions de {@link org.eclipse.rdf4j.query.algebra.StatementPattern}
 * en {@link SubjectPattern}.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public class SubjectPatternCollector extends AbstractQueryModelVisitor<RuntimeException> {
	private Dictionary dictionary;
	private List<Pattern> patterns = new ArrayList<>();

	/*
	 * =============================================================================
	 * === Constructeur
	 * =============================================================================
	 */

	/**
	 * @param dictionary utilisé pour traduire les données en indices
	 */
	public SubjectPatternCollector(Dictionary dictionary) {
		this.dictionary = dictionary;
	}

	/*
	 * =============================================================================
	 * === Accesseurs en lecture
	 * =============================================================================
	 */

	/**
	 * Retourne la liste des patrons rencontrés et traduits.
	 * 
	 * @return la liste des patrons rencontrés et traduits
	 */
	public List<Pattern> getPatterns() {
		return patterns;
	}

	/*
	 * =============================================================================
	 * === Visites
	 * =============================================================================
	 */

	/**
	 * Visite d'un patron pour collectionner sa traduction.
	 * 
	 * @param node le patron visité
	 */
	@Override
	public void meet(StatementPattern node) {
		// récupération des indices du prédicat et de l'objet
		int predicate = dictionary.getIndex(node.getPredicateVar().getValue().stringValue());
		int object = dictionary.getIndex(node.getObjectVar().getValue().stringValue());

		// création et collection du patron
		SubjectPattern pattern = new SubjectPattern(predicate, object);
		patterns.add(pattern);
	}

	/**
	 * Utilise une instance de ce collectionneur pour visiter la requête et retourne
	 * les patrons trouvés.
	 * 
	 * @param node       la requête à traiter
	 * @param dictionary utilisé pour traduire les données en indices
	 * @return la liste des patrons rencontrés et traduits
	 */
	public static List<Pattern> process(QueryModelNode node, Dictionary dictionary) {
		SubjectPatternCollector collector = new SubjectPatternCollector(dictionary);

		/*
		 * Visite de la requête. On sait que la racine n'est pas un StatementPattern
		 * donc on passe sur les enfants directement.
		 */
		node.visitChildren(collector);

		return collector.getPatterns();
	}

}