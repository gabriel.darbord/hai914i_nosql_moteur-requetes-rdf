/**
 * Collectionneurs de patrons.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
package qengine.query.pattern.collector;
