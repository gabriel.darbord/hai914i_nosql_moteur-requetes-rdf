package qengine.query.projection;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import qengine.query.pattern.Pattern;

/**
 * Classe utilitaire pour les projections.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public final class ProjectionUtils {

	/**
	 * Pas d'instanciation.
	 */
	private ProjectionUtils() {
	}

	/*
	 * =============================================================================
	 * === Jointure
	 * =============================================================================
	 */

	/**
	 * Jointure de projections obtenues par
	 * {@link qengine.index.Index#project(Pattern) Index#project()} ou
	 * {@link org.eclipse.rdf4j.model.Model#filter(org.eclipse.rdf4j.model.Resource, org.eclipse.rdf4j.model.IRI, org.eclipse.rdf4j.model.Value, org.eclipse.rdf4j.model.Resource...)
	 * org.eclipse.rdf4j.model.Model#filter()}.
	 * <p>
	 * Les projections sont d'abord ordonnées par taille croissante afin d'optimiser
	 * les jointures. Ces dernières sont réalisées par la méthode
	 * {@link Set#retainAll(java.util.Collection) Set#retainAll()} qui a une
	 * complexité en temps linéaire en la taille du receveur. Ainsi en débutant par
	 * la plus petite projection, on minimise le temps de cette opération car le
	 * receveur sera toujours de taille inférieure ou égale à la première
	 * projection.
	 * 
	 * @param <T>         le type des éléments des projections
	 * @param projections obtenues depuis les patrons de la requête
	 * @return la jointure des projections
	 */
	public static <T> Set<T> join(List<Set<T>> projections) {
		if (projections.isEmpty()) {
			return Collections.emptySet();
		}

		// tri des projections par taille croissante
		projections.sort(new Comparator<>() {
			@Override
			public int compare(Set<T> left, Set<T> right) {
				return left.size() - right.size();
			}
		});

		// récupération de la plus petite projection
		Set<T> join = projections.get(0);

		// on peut arrêter d'évaluer la requête s'il n'y a rien à joindre
		if (projections.size() > 1 && !join.isEmpty()) {
			/*
			 * L'ensemble obtenu est une vue de l'indexage utilisé, mais on va vouloir le
			 * modifier en appliquant des jointures donc il faut créer une copie. Aussi, on
			 * ne la crée que s'il y a des jointures à effectuer.
			 */
			join = new HashSet<>(join);

			// jointure avec les projections suivantes
			for (int i = 1; i < projections.size() && !join.isEmpty(); i++) {
				join.retainAll(projections.get(i));
			}
		}

		return join;
	}

}
