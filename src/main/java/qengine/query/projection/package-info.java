/**
 * Projection de requêtes RDF.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
package qengine.query.projection;
