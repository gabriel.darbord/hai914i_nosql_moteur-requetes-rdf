package qengine.program;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import qengine.engine.AbstractEngine;
import qengine.engine.Engine;
import qengine.engine.comparator.AbstractEngineComparator;

/**
 * Point d'entrée du programme.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public final class Main {

	public static void main(String[] args) {
		// chronométrage du temps d'exécution du programme
		Stopwatch.start("program");

		/*
		 * =============================================================================
		 * === Traitement des arguments
		 * =============================================================================
		 */

		// initialisation de l'analyseur d'arguments
		Options options = configParameters();
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = null;

		try {
			// analyse des arguments
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			// affichage de l'aide et terminaison
			System.err.println(e.getMessage());
			HelpFormatter help = new HelpFormatter();
			help.printHelp("java -jar rdfengine.jar", options, true);
			System.exit(1);
		}

		// récupération des arguments
		String dataPath = cmd.getOptionValue("data");
		String queriesPath = cmd.getOptionValue("queries");
		String outputFolderPath = cmd.getOptionValue("output");

		// nom du fichier de données et du fichier de requêtes
		String dataFileName = new File(dataPath).getName();
		String queriesFileName = new File(queriesPath).getName();

		// correspond aux caractères illégaux pour un nom de fichier
		// on enlève aussi les redondances de '_' pour embellir
		Pattern pattern = Pattern.compile("[" + File.separator + " :.]|__+");

		// chemin du fichier de sortie des résultats
		String outputFileName = "RESULTS_DATA_"//
				+ pattern.matcher(dataFileName).replaceAll("_") + "_QUERIES_"
				+ pattern.matcher(queriesFileName).replaceAll("_") + ".csv";
		String outputPath = outputFolderPath + File.separator + outputFileName;

		// création de l'arborescence du dossier de sortie si nécessaire
		File outputFolder = new File(outputFolderPath);
		outputFolder.mkdirs();

		/*
		 * =============================================================================
		 * === Exécution du moteur
		 * =============================================================================
		 */

		// création du moteur de requêtes
		Engine engine;
		if (cmd.hasOption("compare")) {
			engine = AbstractEngineComparator.Impl.DEFAULT.get(AbstractEngine.Impl.CUSTOM, AbstractEngine.Impl.JENA);
		} else if (cmd.hasOption("jena")) {
			engine = AbstractEngine.Impl.JENA.get();
		} else {
			engine = AbstractEngine.Impl.CUSTOM.get();
		}

		try {
			// analyse des données
			Stopwatch.push("dataParsing");
			engine.parseData(dataPath);
			Stopwatch.pop();

			// traitement des requêtes
			Stopwatch.start("workload");
			Stopwatch.push("queryParsing");
			engine.parseQueries(queriesPath, outputPath);
			Stopwatch.pop();
			Stopwatch.stop("workload");

		} catch (IOException e) {
			System.err.println("Erreur à l'exécution du moteur : " + e.getMessage());
			System.exit(1);
		}

		/*
		 * =============================================================================
		 * === Export des temps d'exécution
		 * =============================================================================
		 */

		// écrivain des temps d'exécution
		try (FileWriter fileWriter = new FileWriter(new File(outputFolder, "TIME" + outputFileName))) {

			// écriture des résultats du chronométrage
			fileWriter.write("nom du fichier de données;" + "nom du dossier des requêtes;" + "nombre de triplets RDF;"
					+ "nombre de requêtes;" + "temps de lecture des données (ms);"
					+ "temps de lecture des requêtes (ms);" + "temps création dico (ms);" + "nombre d’index;"
					+ "temps de création des index (ms);" + "temps total d’évaluation du workload (ms);"
					+ "temps total (du début à la fin du programme) (ms)" + System.lineSeparator()
					// résultats
					+ dataFileName // nom du fichier de données
					+ ";" + queriesFileName // nom du dossier des requêtes
					+ ";" + engine.nbParsedData() // nombre de triplets RDF
					+ ";" + engine.nbProcessedQueries() // nombre de requêtes
					+ ";" + Stopwatch.get("dataParsing") // temps de lecture des données (ms)
					+ ";" + Stopwatch.get("queryParsing") // temps de lecture des requêtes (ms)
					+ ";" + Stopwatch.get("dictionaryCreation") // temps création dico (ms)
					+ ";" + engine.indexSize() // nombre d’index
					+ ";" + Stopwatch.get("indexCreation") // temps de création des index (ms)
					+ ";" + Stopwatch.get("workload") // temps total d’évaluation du workload (ms)
					+ ";" + Stopwatch.stop("program") // temps total (du début à la fin du programme) (ms)
					+ System.lineSeparator());

		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * Définition des paramètres du programme.
	 * 
	 * @return les paramètres du programme
	 */
	private static Options configParameters() {
		Option dataOption = Option.builder("data").hasArg().argName("filepath").desc("chemin du fichier de données")
				.required().build();
		Option queriesOption = Option.builder("queries").hasArg().argName("filepath")
				.desc("chemin du fichier ou dossier de requêtes").required().build();
		Option outputOption = Option.builder("output").hasArg().argName("dirpath")
				.desc("chemin du dossier de sortie du résultat").required().build();
		Option compareOption = Option.builder("compare").desc("comparaison entre notre moteur et RDF4J").build();
		Option jenaOption = Option.builder("jena").desc("utilisation de Jena").build();

		Options options = new Options();
		options.addOption(dataOption);
		options.addOption(queriesOption);
		options.addOption(outputOption);
		options.addOption(compareOption);
		options.addOption(jenaOption);

		return options;
	}

	/**
	 * Pas d'instanciation.
	 */
	private Main() {
	}

}
