package qengine.program;

import java.util.ArrayDeque;
import java.util.HashMap;

/**
 * Un chronomètre qui mémorise des durées en leur associant un nom.
 * <p>
 * En mode simultané avec {@link #start(String)} et {@link #stop(String)}, la
 * durée obtenue n'est pas influencée par d'autres méthodes.
 * <p>
 * En mode séquentiel avec {@link #push(String)} et {@link #pop()}, seule la
 * durée en tête de pile est comptée. De plus, le calcul de cette durée peut
 * être mis en pause avec {@link #pause()} et relancé avec {@link #unpause()}.
 * 
 * @author Pierre Romestant
 * @author Gabriel Darbord
 */
public final class Stopwatch {
	private static long start;
	private static HashMap<String, Long> logs = new HashMap<>();
	private static ArrayDeque<String> stackedLogs = new ArrayDeque<>();

	/**
	 * Pas d'instanciation.
	 */
	private Stopwatch() {
	}

	/*
	 * =============================================================================
	 * === Chronométrage simultané
	 * =============================================================================
	 */

	/**
	 * Démarre le chronomètre pour calculer la durée qui sera associée au nom donné
	 * à l'appel de {@link #stop(String)}.
	 * 
	 * @param name le nom associé à la durée
	 */
	public static void start(String name) {
		logs.put(name, System.nanoTime());
	}

	/**
	 * Enregistre et retourne la durée écoulée depuis l'appel à
	 * {@link #start(String)} avec le nom donné.
	 * 
	 * @param name le nom associé à la durée
	 * @return une durée en millisecondes
	 */
	public static long stop(String name) {
		long end = System.nanoTime();
		long start = logs.get(name);
		long duration = end - start;
		logs.put(name, duration);
		return nanoToMilli(duration);
	}

	/*
	 * =============================================================================
	 * === Chronométrage séquentiel
	 * =============================================================================
	 */

	/**
	 * Démarre le chronomètre pour calculer la durée qui sera associée au nom donné
	 * à l'appel de {@link #pop()}. Il sera mis en pause par le prochain appel à
	 * cette méthode, jusqu'à ce qu'il revienne en tête de pile.
	 * 
	 * @param name le nom associé à la durée
	 */
	public static void push(String name) {
		// s'il y a déjà un chrono en cours, il est mis en pause
		if (!stackedLogs.isEmpty()) {
			pause();
		}

		// ajout de l'entrée à la pile et début du chrono
		stackedLogs.push(name);
		start = System.nanoTime();
	}

	/**
	 * Enregistre la durée écoulée pour le chronomètre de l'appel à
	 * {@link #push(String)} correspondant, puis le chronomètre de la nouvelle tête
	 * de pile est relancé.
	 */
	public static void pop() {
		// ajout de la durée à l'entrée en cours de chronométrage
		long end = System.nanoTime();
		long duration = end - start;
		put(stackedLogs.pop(), duration);

		// reprise du chronométrage
		start = System.nanoTime();
	}

	/**
	 * Met en pause le chronomètre de l'appel à {@link #push(String)} en tête de
	 * pile.
	 */
	public static void pause() {
		long end = System.nanoTime();
		long duration = end - start;
		put(stackedLogs.peek(), duration);
	}

	/**
	 * Redémarre le chronomètre de l'appel à {@link #push(String)} en tête de pile.
	 */
	public static void unpause() {
		start = System.nanoTime();
	}

	/*
	 * =============================================================================
	 * === Accesseurs des durées
	 * =============================================================================
	 */

	/**
	 * Retourne la durée en millisecondes associée au nom donné.
	 * 
	 * @param name le nom associé à la durée
	 * @return une durée en millisecondes
	 */
	public static long get(String name) {
		long duration = logs.get(name);
		return nanoToMilli(duration);
	}

	/**
	 * Accumule la durée associée au nom donné.
	 * 
	 * @param name     le nom associé à la durée
	 * @param duration une durée en nanosecondes
	 */
	public static void put(String name, long duration) {
		logs.compute(name, (key, val) -> val == null ? duration : val + duration);
	}

	/*
	 * =============================================================================
	 * === Conversion d'unité
	 * =============================================================================
	 */

	/**
	 * Traduit une valeur en nanosecondes vers une valeur en millisecondes.
	 * 
	 * @param value une valeur en nanosecondes
	 * @return une valeur en millisecondes
	 */
	public static long nanoToMilli(long value) {
		return value / 1_000_000L;
	}

}
